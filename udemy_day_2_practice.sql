-- Q1
select
	count(*)
from
	payment
where
	customer_id=100;


-- Q2
select
	last_name
from
	customer
where
	first_name='ERICA';


-- Q3
select
	count(*)
from
	rental
where
	return_date is null;


-- Q4
select
	payment_id,
	amount
from
	payment
where
	amount <= 2;



-- Q5
select
	*
from
	payment
where
	(amount < 2 or amount >10) and
	(customer_id = 322 or
	customer_id = 346 or
	customer_id = 354)
order by
	customer_id asc,
	amount desc;


-- Q6
select
	count(*)
from
	payment
where
	amount between 1.99 and 2.99
	and payment_date between '2020-01-26' and '2020-01-28';


-- Q7
select
	*
from
	payment
where
	customer_id in (12,25,67,93,124,234) and
	amount in (4.99, 7.99, 9.99) and
	payment_date between '2020-01-01' and '2020-02-01'


-- Q8
select
	count(*)
from
	customer
where
	first_name like '___' and
	(last_name like '%X' or last_name like '%Y')


-- Q9
select
	count(*) as no_of_movies
from
	film
where
	description like '%Saga%' and
	(title like 'A%' or
	title like '%R');


-- Q10
select
	*
from
	customer
where
	first_name like '%ER%' and
	first_name like '_A%'
order by
	last_name desc;


-- Q11
select
	count(*)
from
	payment
where
	(amount = 0 or
	amount between 3.99 and 7.99) and
	payment_date between '2020-05-01' and '2020-05-02';
