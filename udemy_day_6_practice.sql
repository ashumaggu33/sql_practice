-- Q1
select
	b.seat_no,
	count(*)
from
	boarding_passes b
left join
	seats s
on
	b.seat_no = s.seat_no
group by
	b.seat_no
order by
	count(*) desc


-- Q2
select
	case
		when(b.seat_no like '%A') then 'A'
		when(b.seat_no like '%B') then 'B'
		when(b.seat_no like '%C') then 'C'
		when(b.seat_no like '%D') then 'D'
		when(b.seat_no like '%E') then 'E'
		when(b.seat_no like '%F') then 'F'
		when(b.seat_no like '%G') then 'G'
		when(b.seat_no like '%H') then 'H'
	end as seat,
	count(*)
from
	boarding_passes b
left join
	seats s
on
	b.seat_no = s.seat_no
group by
	seat
order by
	count(*) desc

or

select
	RIGHT(s.seat_no, 1),
	count(*)
from
	boarding_passes b
left join
	seats s
on
	b.seat_no = s.seat_no
group by
	RIGHT(s.seat_no, 1)
order by
	count(*) desc


-- Q3
select
	c.first_name,
	c.last_name,
	a.phone,
	a.district
from
	customer c
right join
	address a
on
	a.address_id = c.address_id
where
	lower(a.district)='texas';


-- Q4
select
	a.*
from
	customer c
right join
	address a
on
	a.address_id = c.address_id
where
	c.customer_id is null;


-- Q5
select
	t.ticket_no,
	t.flight_id,
	t.fare_conditions,
	round(avg(t.amount), 2)
from
	boarding_passes b
right join
	ticket_flights t
on
	t.ticket_no = b.ticket_no and
	t.flight_id = b.flight_id
group by
	t.fare_conditions,
	t.ticket_no,
	t.flight_id