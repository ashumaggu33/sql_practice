-- Q1
select
	lower(first_name),
	lower(last_name),
	email
from
	customer
where
	length(first_name) > 10 or
	length(last_name) > 10

-- Q2
select
	RIGHT(email, 5),
	left(RIGHT(email, 4), 1)
from
	customer
where
	email like '%.org'


-- Q3
select
    name || " - " || category || ": $"|| price as product_summary
from
    products
order by
    name;


-- Q4
select
	LEFT(email, 1) || '***' || RIGHT(email, 19) as anonymised_column
from
	customer
where
	email like '%@sakilacustomer.org'


-- Q5
select
 	last_name || ', ' ||left(email, position('.' in email) - 1)
from
	customer


-- Q6
select
	left(email, 1)
	|| '***'
	|| substring(email from position('.' in email) for 2)
	|| '***'
	|| substring(email from position('@' in email))
from
	customer


-- Q7
select
	'***'
	|| substring(email from position('.' in email)-1 for 3)
	|| '***'
	|| substring(email from position('@' in email))
from
	customer


-- Q8
select
	sum(amount) as am,
	extract(month from payment_date) as mon
from
	payment
group by
	mon
order by
	am desc


-- Q9
select
	sum(amount) as am,
	extract(dow from payment_date) as mon
from
	payment
group by
	mon
order by
	am desc


-- Q10
select
	customer_id,
	sum(amount) as am,
	extract(week from payment_date) as mon
from
	payment
group by
	mon, customer_id
order by
	am desc


-- Q11
select
	sum(amount) as am,
	to_char(payment_date, 'Dy, DD/mm/yyyy') as dm
from
	payment
group by
	dm
order by
	am


-- Q12
select
	sum(amount) as am,
	to_char(payment_date, 'Mon, YYYY') as dm
from
	payment
group by
	dm
order by
	am

-- Q13
select
	sum(amount) as am,
	to_char(payment_date, 'Dy, HH:mi') as dm
from
	payment
group by
	dm
order by
	am