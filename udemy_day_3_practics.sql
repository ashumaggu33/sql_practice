-- Q1
select
	min(replacement_cost),
	max(replacement_cost),
	round(avg(replacement_cost), 2),
	sum(replacement_cost)
from
	film;

-- Q2
select
	staff_id,
	sum(amount) as sum_amount,
	count(*)
from
	payment
group by
	staff_id
order by
	sum_amount desc
limit 2;


-- Q3
select
	staff_id,
	sum(amount) as sum_amount,
	count(*)
from
	payment
where
	amount != 0
group by
	staff_id
order by
	sum_amount desc
limit 2;


-- Q4
select
	staff_id,
	sum(amount) as highest_sale_amount,
	date(payment_date),
	count(*)
from
	payment
group by
	staff_id, date(payment_date)
order by
	highest_sale_amount desc;


-- Q5
select
    city,
    avg(Amount) as AverageAmount
from
    Sales
group by
    city
having
    AverageAmount > 150 and count(transactionId) > 2
order by
    AverageAmount desc;


-- Q6
select
	customer_id,
	round(avg(amount),2) as average_amount,
	date(payment_date) as payment_day,
	count(*)
from
	payment
group by
	customer_id, payment_day
having
	date(payment_date) between '2020-04-28' and '2020-05-01' and
	count(customer_id) > 1
order by
	average_amount desc
