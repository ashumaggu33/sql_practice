-- 1.From purchase_order table write a SQL query to calculate total purchase amount of all orders. Return total purchase amount.

select
	sum(purch_amt) as "total puchase amount"
from
	purchase_order;


-- 2.From purchase_order table, write a SQL query to calculate the average purchase amount of all orders. Return average purchase amount.

select
	round(avg(purch_amt), 2) as "average purchase amount"
from
	purchase_order;

-- 3.From purchase_order table, write a SQL query that counts the number of unique salespeople. Return number of salespeople.

select
	count(distinct(salesman_id)) as "number of distinct salespeople"
from
	purchase_order;


-- 4. From the customer table, write a SQL query to determine the number of customers who received at least one grade for their activity.

select
	*
from
	customer
group by
	customer_id
having
	count(grade) >=2;

-- 5. From the purchase_order table, write a SQL query to find the maximum purchase amount.

select
	max(purch_amt) as "maximum purchase amount"
from
	purchase_order;

-- 6. From the purchase_order table, write a SQL query to find the minimum purchase amount.

select
	min(purch_amt) as "minimum purchase amount"
from
	purchase_order;

-- 7. From the customer table, write a SQL query to find the highest grade of the customers in each city. Return city, maximum grade.

select
	city,
	max(grade) as "maximum grade"
from
	customer
group by
	city;

select * from customer;

-- 8. From the purchase_order table, write a SQL query to find the highest purchase amount ordered by each customer.
-- Return customer ID, maximum purchase amount.

select
	customer_id as "customer ID",
	max(purch_amt) as "maximum purchase amount"
from
	purchase_order
group by
	customer_id;

select * from purchase_order

-- 8 Question : 5 Mins each question = 40 Mins (All done)

