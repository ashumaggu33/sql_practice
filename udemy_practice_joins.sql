select
	t.ticket_no, f.scheduled_departure
from
	ticket_flights as tf
inner join
	tickets as t
on
	tf.ticket_no = t.ticket_no
inner join
	flights as f
on
	f.flight_id = tf.flight_id



-- Q2
select
	cu.first_name,
	cu.last_name,
	cu.email,
	co.country
from
	customer cu
inner join
	address a
on
	cu.address_id = a.address_id
inner join
	city c
on
	c.city_id = a.city_id
inner join
	country co
on
	co.country_id = c.country_id
where
	lower(co.country) = 'brazil'


-- Q3
select
	passenger_name,
	sum(total_amount) as total_amount
from
	tickets t
inner join
	bookings b
on
	t.book_ref = b.book_ref
group by
	passenger_name
order by
	total_amount desc


-- Q4
select
    passenger_name,
    fare_conditions,
	count(*) as unique_record
from
	tickets t
inner join
	ticket_flights tf
on
	tf.ticket_no = t.ticket_no
where
	passenger_name like '%ALEKSANDR IVANOV%'
group by
	passenger_name, fare_conditions


-- Q5
select
	first_name,
	last_name,
	title,
	count(*)
from
	customer c
inner join
	rental r
on
	r.customer_id = c.customer_id
inner join
	inventory i
on
	i.inventory_id = r.inventory_id
inner join
	film f
on
	f.film_id = i.film_id
group by
	title,
	first_name,
	last_name
order by
	4 desc
