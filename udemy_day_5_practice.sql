-- Q1
select
    product_name,
    sum(quantity_sold * price_per_unit) as total_revenue
from
    sales
group by
    product_name
order by
    total_revenue desc
limit 1;


-- Q2
select
	film_id,
	rental_rate,
	replacement_cost,
	round(round(rental_rate/replacement_cost,4)*100,2) as increased_rate
from
	film
where
	round(round(rental_rate/replacement_cost,4)*100,2) < 4
order by 4

-- Q3
select
    order_id,
    product_id,
    quantity,
    unit_price,
case
    when quantity > 1 then (unit_price - round(0.10*unit_price, 2))*quantity+shipping_fee
    else unit_price+shipping_fee
end as total_price
from
    sales_orders


-- Q4
select
	--distinct(to_char(scheduled_departure, 'Mon')) as mont
	count(*),
	case
		when to_char(scheduled_departure, 'Mon') in ('Dec', 'Jan', 'Feb') then 'Winter'
		when to_char(scheduled_departure, 'Mon') in ('Mar', 'Apr', 'May') then 'Spring'
		when to_char(scheduled_departure, 'Mon') in ('Jun', 'Jul', 'Aug') then 'Summer'
		else 'Fall'
	else 'invalid'
	end as season
from
	flights
group by
	season


-- Q5
select
	title,
	case
		when (rating in ('PG', 'PG-13') or length > 210) then 'Great rating or long (tier 1)'
		when description like '%Drama%' and length > 90 then 'Long Drama(tier 2)'
		when description like '%Drama%' and length < 90 then 'Short Drama (tier 3)'
		when rental_rate < 1 then 'Very Cheap(tier 4)'
	end as tier
from
	film
where
	case
		when (rating in ('PG', 'PG-13') or length > 210) then 'Great rating or long (tier 1)'
		when description like '%Drama%' and length > 90 then 'Long Drama(tier 2)'
		when description like '%Drama%' and length < 90 then 'Short Drama (tier 3)'
		when rental_rate < 1 then 'Very Cheap(tier 4)'
	end is not null


